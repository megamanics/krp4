First, we need to set up the Pi4 as the gadget using the dwc2 driver in device tree overlay, then load it up.

Add dtoverlay=dwc2 to the /boot/config.txt
Add modules-load=dwc2 to the end of /boot/cmdline.txt
Next, we need to enable the libcomposite driver. You might see some tutorials using the g_* series of drivers (eg g_ether) but for these you can only load one at a time whereas libcomposite allows you to load up whichever is needed through configuration, so libcomposite is the better choice (in my opinion).

Add libcomposite to /etc/modules
The next step is where we configure our gadget. To do this we set up a script to run when booting up. In this script we will do the following:

Create a directory to represent the gadget
Set up the various gadget parameters including the vendor ID, product ID etc
Under the directory create the configurations and instantiate the USB function we need. In this case we will use the the ECM (Ethernet Control Model)
Associate the function to the configuration with symbolic links
Activate the gadget by writing the UDC name (in /sys/class/udc) to the UDC attribute
Create /root/usb.sh to implement the steps above.
After this make sure the script is executable and set it up to run when the Pi4 is started.

Make /root/usb.sh executable with chmod +x /root/usb.sh
Add /root/usb.sh to /etc/rc.local before exit 0
Set up the networking for usb0
Your Pi4 is now configured as an ethernet gadget with the interface usb0 but we still need to make sure the rest of the networking configurations are set up properly.

Dnsmasq is an open source project that provides network infrastructure for small networks and we’ll use it for the really small network between the iPad Pro and the Pi4. It includes DNS, DHCP, router advertisement and network boot and is widely used for tethering on smartphones and portable hotspots.

Install dnsmasq with sudo apt-get install dnsmasq
Once you have installed dnsmasq, we need to set up it up properly for usb0.

Create the file /etc/dnsmasq.d/usb with following:
interface=usb0
dhcp-range=192.168.100.2,192.168.100.200,255.255.255.248,1h
dhcp-option=3
leasefile-ro
Now that we have dnsmasq we don’t need the standard DHCP client to muck around with usb0.

Add denyinterfaces usb0 to /etc/dhcpcd.conf
The final step is to set up the network interface for usb0.

Create /etc/network/interfaces.d/usb0 with the following:
auto usb0
allow-hotplug usb0
iface usb0 inet static
  address 192.168.100.1
  netmask 255.255.255.248
